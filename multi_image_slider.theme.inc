<?php

/**
 * Theme function for BX-Slider.
 */
function theme_multi_image_slider($variables) {
  $result = $image_style = $image_pager_style = '';
  $result ='<div class="multi-image-holder">';
  $result .= '<ul id = "ul_'.$variables['entity_id'].'" class="bxslider">';
  if(isset($variables['bx_image'])) {
    $image_style = variable_get('multi_image_slider_image_style','large');
    $image_pager_style = variable_get('multi_image_slider_image_thumb_style','thumbnail');
    foreach($variables['bx_image'] as $bx_image_data) {
      $bx_image_uri = image_style_url($image_style, $bx_image_data['bx_image_uri']);
      if(module_exists('lightbox2') && variable_get('multi_image_slider_lightbox','yes') == 'yes') {        
        $result .= "<li class = 'image_thumb'><a title = '".$bx_image_data['bx_image_title']."' href='".$bx_image_uri."'   
        rel=lightbox[".$variables['entity_id']."]><img src=".$bx_image_uri." title = '".$bx_image_data   
        ['bx_image_title']."'></a></li>";
      }
      else {
        $result .= "<li class = 'image_thumb'><img src=".$bx_image_uri." title = '".$bx_image_data['bx_image_title']."'></
        li>";
      }
    }
  }
  $result .= '</ul>';
  if(variable_get('multi_image_slider_pager','yes') != 'no') {
    $result .= '<div class="bx-pager" id = "pager_'.$variables['entity_id'].'" >';
    $count = 0;
    if(isset($variables['bx_image'])) {
      foreach($variables['bx_image'] as $bx_image_thumb) {
        $bx_image_thumb_uri = image_style_url($image_pager_style, $bx_image_thumb['bx_image_uri']);   
        $result .= "<a data-slide-index=".$count." href=''><img src=".$bx_image_thumb_uri."></a>"; 
        $count = $count +1;
      }
    }
    $result .= '</div>';
  }
  $result .= '</div>';
  return $result;
}
