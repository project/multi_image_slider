﻿INTRODUCTION
------------
This Module is used for showing images(Image Fields) in the bx slider format with pager as a image.

You need to select Format "Multi Image Slider" for relevant image field in manage dispaly page for the relevant content type.

multi_image_slider.module: showing image in bx slider format of your image style choice with pager and controls.

Available options

    Image style
    Caption	
    Lightbox (to enlarge images)
    Pager (Image as thumbnails)
    Prev/Next controls

REQUIREMENTS
------------
This module requires node module. node module should be enable.

INSTALLATION
------------
Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

CONFIGURATION
------------
Go to configuration link in admin menu.
Configuration -> Content authoring -> Multi Image Slider
 
Admininistrator USAGE
------------
Assign permission "Administer Multi Image Slider" to the administrator.
Configuration -> Content authoring -> Multi Image Slider.
Select image style for image as well as pager.
Save Configuration.

Content USAGE
------------
Go to Content Type in the structure menu.
Select the node type or content type.
Add cck field of image type in manage field page.
Go to manage dispaly page for the relevant content type.
select Format "Multi Image Slider" for relevant image field.

MAINTAINERS
-----------
Current maintainers:
 * Chetan Sharma (chetan-sharma) - https://www.drupal.org/u/chetan-sharma
