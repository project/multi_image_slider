/**
 * @file
 * Multi Image Slider script for Bx slider.
 */

(function ($) {
  Drupal.behaviors.multi_image_holder = {
    attach: function (context, settings) {
      var slider_mode = settings.multi_image_slider.slider_mode;
      if(settings.multi_image_slider.slider_control == 'false') var slider_control = false;
      else var slider_control = true;
      if(settings.multi_image_slider.slider_autoslide == 'false') var slider_autoslide = false;
      else var slider_autoslide = true;
      if(settings.multi_image_slider.slider_caption == 'false') var slider_caption = false;
      else var slider_caption = true;
      var mode_speed = settings.multi_image_slider.mode_speed;
      var slidewidth = settings.multi_image_slider.slidewidth;
      var number_image = settings.multi_image_slider.number_image;
      if(settings.multi_image_slider.infiniteLoop == 'false') var infiniteLoop = false;
      else var infiniteLoop = true;
      if(settings.multi_image_slider.slider_pager == 'yes') {
        number_image = 1;
        infiniteLoop = false;
      }
      if(settings.multi_image_slider.adaptiveHeight == 'false') var adaptiveheight = false;
      else var adaptiveheight = true;
      var pagertype = settings.multi_image_slider.pagertype;
      var sliderSets = jQuery('.multi-image-holder');
      jQuery(sliderSets).each(function() {
        var targetSlider = null;
        var targetSlider = "#" + jQuery(this).children('.bxslider').attr('id');
        if(settings.multi_image_slider.slider_pager == 'yes') var targetPager = "#" + jQuery(this).children(
        '.bx-pager').attr('id');
        var bslider = jQuery(targetSlider).bxSlider({
          video: true,
          controls: slider_control,
          mode: slider_mode,
          speed: mode_speed,
          minSlides: number_image,
          maxSlides: number_image,
          slideWidth: slidewidth,
          auto: slider_autoslide,
          captions: slider_caption,
          pagerCustom: targetPager,
          pagerType: pagertype,
          infiniteLoop: infiniteLoop,
          adaptiveHeight: adaptiveheight,
          useCSS: true
        }); 
      });
       
    }
  }
}(jQuery));
