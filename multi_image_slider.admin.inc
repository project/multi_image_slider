<?php

/**
 * @file
 * Code that is only expected to run when administrators are viewing the site.
 */

/**
 * Returns the page for configuring the multi image slider settings.
 */
function multi_image_slider_settings_page() {
  return drupal_get_form('multi_image_slider_settings_form');
}

/**
 * Returns a form for configuring the multi image slider settings.
 */

function multi_image_slider_settings_form() {
  $form = array();
  $slider_mode = array('horizontal' => 'Horizontal','vertical' => 'Vertical','fade' => 'Fade');
  $slider_control = array('true'=>'True','false'=>'False');
  $form['group_image_style'] = array(
    '#type' => 'fieldset',
    '#title' => t('Image Style'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,  
  );
  $form['group_image_style']['multi_image_slider_image_style'] = array(
    '#type' => 'select',
    '#title' => t('Select Image Style'),
    '#description' => t('Select the image style for show images in Multi Image Slider.'),
    '#options' => image_style_options(),
    '#default_value' => variable_get('multi_image_slider_image_style','large'),
  );
  $form['group_image_style']['multi_image_slider_pager'] = array(
    '#type' => 'select',
    '#title' => t('Show image in pager style'),
    '#description' => t('Show Pager in Image Style.'),
    '#options' => array('yes'=>'Yes','no'=>'No'),
    '#default_value' => variable_get('multi_image_slider_pager','yes'),
  );
  $form['group_image_style']['multi_image_slider_image_thumb_style'] = array(
    '#type' => 'select',
    '#title' => t('Select Pager Style'),
    '#description' => t('Select the image pager style for show images in Multi Image Slider.'),
    '#options' => image_style_options(),
    '#default_value' => variable_get('multi_image_slider_image_thumb_style','thumbnail'),
  );
  $form['group_image_style']['multi_image_slider_control'] = array(
    '#type' => 'select',
    '#title' => t('Show Pager Control'),
    '#description' => t('Show Multi Image Slider Control.'),
    '#options' => $slider_control,
    '#default_value' => variable_get('multi_image_slider_control','true'),
  );
  if(module_exists('lightbox2')) {
    $form['group_image_style']['multi_image_slider_lightbox'] = array(
      '#type' => 'select',
      '#title' => t('Open Image in Lightbox'),
      '#description' => t('Open Image in Lightbox.'),
      '#options' => array('yes'=>'Yes','no'=>'No'),
      '#default_value' => variable_get('multi_image_slider_lightbox','yes'),
    );
  }
  $form['group_image_style']['multi_image_slider_pagertype'] = array(
    '#type' => 'select',
    '#title' => t('Thumbnail pager type'),
    '#description' => t('Select Thumbnail pager type.'),
    '#options' => array('full'=>t('Full'),'short'=>t('Short')),
    '#default_value' => variable_get('multi_image_slider_pagertype','full'),
    '#states' => array(
      'visible' => array(
        ':input[name="multi_image_slider_pager"]' => array('value' => 'no'),
      ),
    ),
  );
  $form['group_slider_caption'] = array(
    '#type' => 'fieldset',
    '#title' => t('Slider Caption'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,  
  );
  $form['group_slider_caption']['multi_image_slider_caption'] = array(
    '#type' => 'select',
    '#title' => t('Show Slider Caption'),
    '#description' => t('Show Multi Image Slider Caption.'),
    '#options' => $slider_control,
    '#default_value' => variable_get('multi_image_slider_caption','false'),
  );
  $form['group_multi_image_slide'] = array(
    '#type' => 'fieldset',
    '#title' => t('Multi Image Slider'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,  
  );
  $form['group_multi_image_slide']['multi_image_slider_mode'] = array(
    '#type' => 'select',
    '#title' => t('Select Multi Image Slide Direction'),
    '#description' => t('Select Multi Image Slide Direction.'),
    '#options' => $slider_mode,
    '#default_value' => variable_get('multi_image_slider_mode','horizontal'),
  );
  $form['group_slider_other'] = array(
    '#type' => 'fieldset',
    '#title' => t('Other'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,  
  );
  $form['group_slider_other']['multi_image_slider_mode_speed'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide transition duration'),
    '#size' => 6,
    '#description' => t('Slide transition duration (in ms).'),
    '#default_value' => variable_get('multi_image_slider_mode_speed','500'),
    '#element_validate' => array('element_validate_integer_positive'),
  );
  $form['group_multi_image_slide']['multi_image_slider_autoslide'] = array(
    '#type' => 'select',
    '#title' => t('Slider Autoslide'),
    '#description' => t('Multi Image Slider Autoslide.'),
    '#options' => $slider_control,
    '#default_value' => variable_get('multi_image_slider_autoslide','false'),
  );
  $form['group_slider_other']['multi_image_slider_infiniteLoop'] = array(
    '#type' => 'select',
    '#title' => t('Slider transition in InfiniteLoop'),
    '#description' => t('clicking "Next" while on the last slide will transition to the first slide and vice-versa.'),
    '#options' => $slider_control,
    '#default_value' => variable_get('multi_image_slider_infiniteLoop','false'),
    '#states' => array(
      'visible' => array(
        ':input[name="multi_image_slider_pager"]' => array('value' => 'no'),
      ),
    ),
  );
  $form['group_slider_other']['multi_image_slider_adaptiveHeight'] = array(
    '#type' => 'select',
    '#title' => t('Use Adaptive Height'),
    '#description' => t('Dynamically adjust slider height based on each slide\'s height.'),
    '#options' => $slider_control,
    '#default_value' => variable_get('multi_image_slider_adaptiveHeight','false'),
  );
  $form['group_multi_image_slide']['multi_image_slider_slideWidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Image Slider slideWidth'),
    '#size' => 6,
    '#description' => t('Image Slider slideWidth.'),
    '#default_value' => variable_get('multi_image_slider_slideWidth','500'),
    '#element_validate' => array('element_validate_integer_positive'),
  );
  $form['group_multi_image_slide']['multi_image_slider_number_image'] = array(
    '#type' => 'select',
    '#title' => t('Slider Image Count'),
    '#description' => t('Number of slides to be shown on the page worked only if "pager in image style is No".'),
    '#options' => array('1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'9'),
    '#default_value' => variable_get('multi_image_slider_number_image','1'),
    '#states' => array(
      'visible' => array(
        ':input[name="multi_image_slider_pager"]' => array('value' => 'no'),
      ),
    ),
  );
  return system_settings_form($form);
}
